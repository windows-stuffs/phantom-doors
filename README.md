
# Scripts to change doors to phantom mode and back

Most camps / forts are designed  for raid ratio 2:1. Without this ratio it is almost impossible to lockpick the doors /gates. So there are a lot a small groups, that cannot do raids / rescues, because they haven't the numbers to have a reasonable chance.
Example: In a camp / fort  are 8 defenders. With a raid ratio 2:1,  the raiders need to have 14- 16 attackers. Most groups can't bring in that many.

A solution could be to have open camps with raid ratio 1:1. The drawback of this solution is that when there are not good fighters role playing in the camp, they might be attacked over and over by combat  groups. With the risk they don't want to role play in the camp anymore.

Most of the groups have put a lot of effort in building a nice camp / fort and they do not want to change the building. They want to keep the doors so less good fighters can roleplay in the camp and/or for raids  with the raid ratio 2:1

A better solution is using scripts  to change the door to  phantom mode and back.
These scrips can be used for all doors including Kool door systems.

## How to use these scripts?

### Common setting for all the scripts:

All scripts have some common settings. The first is the channel, where the scripts are listening / sending messages. It strongly recommended to change this channel. You can use integers from -2147483648 to 2147483647.

For instance:

```
integer CHANNEL = 1234;
```
The channel, where this script listening (number range −2147483648 to 2147483647)

```
string  PASSWORD = "Some Secure Password";
```
It is  strongly recommend to set a password

**IMPORTANT:** These values need to be the same in all scripts.


**Phantom door listener.lsl**

This script is intended for doors that we want to change to phantom and back. Simply place this script in the doors contents. You can to change the value of the `ALPHA` when the phantom is enabled (0.0 = full invisible, 0.5 = half visible, 1.0 = full visible).

**IMPORTANT:** Do not forget change the permissions of the script to no-modify..

### Senders

Basically there are 3 basic options for the senders:

* `Sender - phantom door (check the distance).lsl` - Everyone can change the settings. This option is not recommended. You can also change the value of `MAX_DISTANCE` (Default is set to 3 meters), the `COLOR_ENABLED` and `COLOR_DISABLED` with the RGB (red, green, blue) values.

* `Sender - phantom door (active group and the distance).lsl` - Everyone, who is a member of a particular group can change the settings. This option is most likely for using the scripts. You can also change the value of `MAX_DISTANCE` (Default is set to 3 meters), the `COLOR_ENABLED` and `COLOR_DISABLED` with the RGB (red, green, blue) values. You need to set the Group of the prim to the group, that is allowed use this sender.

* `Sender - phantom door (HUD).lsl` - Only they who own this HUD can to change the settings of the doors, This is also appropriate with a combination of the previous script. You can also change the value of the `COLOR_ENABLED` and `COLOR_DISABLED` with the RGB (red, green, blue) values. You can put this script inside some prim contents and then attach this prim like a HUD.

**IMPORTANT:** Do not forget to set the permissions  of the scripts to avoid security issues - Minimal No-Modify!

If you have any questions regarding these scripts feel free to contact inworld
*Ragnar (Pavell Clowes)*

**Editing:** *Mariko Marchant*
