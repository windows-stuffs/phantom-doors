// *****************************
// **  Phantom Door listener  **
// *****************************
// License - GNU GPL
// Version: 1.1.0
// ------------------------------------------------------------
// ------------------------------------------------------------

// This script is intended for doors that we want to change to phantom and back.
// Simply place this script in the doors contents. 
// If it is a linked door, you will probably need put this script in the linked
// part of the door.

// ------------------------------------------------------------
// ------------------------------------------------------------

integer CHANNEL = 1234;                     // The channel, where this script listening (number range −2147483648 to 2147483647)
string  PASSWORD = "Some Secure Password";  // Set here some secure password
float ALPHA = 0.5;                          // Set the aplha channel when the phantom is enabled
                                            // (0.0 = full invisible, 0.5 = half visible, 1.0 = full visible)

// ------------------------------------------------------------
// ------------------------------------------------------------

integer listenHandle;

// ------------------------------------------------------------

default
{
    state_entry()
    {
        llSetMemoryLimit(8192);
        llListenRemove(listenHandle);
        llSetStatus(STATUS_PHANTOM, FALSE);
        llSetAlpha( 1.0, ALL_SIDES );
        listenHandle = llListen(CHANNEL, "", "", "");
    }   // End state_entry()

    on_rez(integer start_param)
    {
        llResetScript();
    }   // End on_rez(integer start_param)
    
    changed(integer change)
    {
        if (change & CHANGED_OWNER) {
            llResetScript();
        }
    }   // End changed(integer change)
    
    listen( integer channel, string name, key id, string message )
    {
        if (message == PASSWORD + ":ENABLE") {
            llSetStatus(STATUS_PHANTOM, TRUE);
            llSetAlpha( ALPHA, ALL_SIDES );
        }
        else if (message == PASSWORD + ":DISABLE") {
            llSetStatus(STATUS_PHANTOM, FALSE);
            llSetAlpha( 1.0, ALL_SIDES );
        }
    }   // End listen( integer channel, string name, key id, string message )

}   // End state default

// ------------------------------------------------------------
// ------------------------------------------------------------
