// **************************************************
// **  Sender - phantom door (check the distance)  **
// **************************************************
// License - GNU GPL
// Version: 1.1.0
// ------------------------------------------------------------
// ------------------------------------------------------------

// This script allow to everyone, who is in the distance of the prim max. 3 meters
// (the default settings), can switch the doors to the phantom mode and back.
// Basically we not recommend using this script.

// ------------------------------------------------------------
// ------------------------------------------------------------

integer CHANNEL = 1234;                     // The channel, where this script listening (number range −2147483648 to 2147483647)
string  PASSWORD = "Some Secure Password";  // Set here some secure password
float MAX_DISTANCE = 3.0;                   // The max. distance (meters) of the prim, which allow to change the settings of the doors
vector COLOR_ENABLED  = <255, 0, 0>;        // Color (RGB) of the server, when the phantom doors is enabled (default Red color)
vector COLOR_DISABLED = <0, 255, 0>;        // Color (RGB) of the server, when the phantom doors is enabled (default Green color)

// ------------------------------------------------------------

integer status = 0;
integer listenHandle;

// ------------------------------------------------------------

setStatus(integer stat)
{
    status = stat;
    if (status == 1) {
        llSetColor(COLOR_ENABLED/255, ALL_SIDES);
        llSetText("Phantom Doors System\nis enabled", COLOR_ENABLED/255, 1.0);
    }
    else {
        llSetColor(COLOR_DISABLED/255, ALL_SIDES);
        llSetText("Phantom Doors System\nis disabled", COLOR_DISABLED/255, 1.0);
    }    
}   // End setStatus(integer stat)

// ------------------------------------------------------------

default
{
    state_entry()
    {
        llSetMemoryLimit(12228);
        llListenRemove(listenHandle);
        listenHandle = llListen(CHANNEL, "", "", "");
        setStatus(status);
        llRegionSay(CHANNEL, PASSWORD + ":DISABLE");
    }   // End state_entry()

    on_rez(integer start_param)
    {
        llResetScript();
    }   // End on_rez(integer start_param)
    
    changed(integer change)
    {
        if (change & CHANGED_OWNER) {
            llResetScript();
        }
    }   // End changed(integer change)

    touch_start(integer total_number)
    {
        key avi = llDetectedKey(0);
        if (llVecDist(llDetectedPos(0), llGetPos()) < MAX_DISTANCE) {
            if (status == 1) {
                llRegionSay(CHANNEL, PASSWORD + ":DISABLE");
                llShout(0, "\n" + llKey2Name(avi) + " used the door changer.\n┌────────────────────────────┐\n         All these doors is closed.\n    You will need to lockpick them!\n└────────────────────────────┘");
            }
            else {
                llRegionSay(CHANNEL, PASSWORD + ":ENABLE");
                llShout(0, "\n" + llKey2Name(avi) + " used the door changer.\n┌────────────────────────┐\n   All these doors is opened.\n          The way is free!!\n└────────────────────────┘");
            }
            status = ! status;
            setStatus(status);
        }
        else {
            llRegionSayTo(avi, 0, "Your avatar is far away for change the door system !!!");
        }
        llSleep(1);
    }   // End touch_start(integer total_number)

    listen( integer channel, string name, key id, string message )
    {
        if (message == PASSWORD + ":ENABLE") {
            setStatus(1);
        }
        else if (message == PASSWORD + ":DISABLE") {
            setStatus(0);
        }
        else if (message == PASSWORD + ":STATUS") {
            llRegionSay(CHANNEL, PASSWORD + ":" + (string)status);
        }
    }   // End listen( integer channel, string name, key id, string message )

}   // End state default

// ------------------------------------------------------------
// ------------------------------------------------------------