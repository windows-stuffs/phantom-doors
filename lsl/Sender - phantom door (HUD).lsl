// ***********************************
// **  Sender - phantom door (HUD)  **
// ***********************************
// License - GNU GPL
// Version: 1.1.0
// ------------------------------------------------------------
// ------------------------------------------------------------

// This script is for the HUD button, which can to change the settings of the doors.
// This is also appropriate with a combination of the another sender scripts.
// For make the HUD button you can put this script inside some prim contents and 
// then attach this prim like a HUD.

// ------------------------------------------------------------
// ------------------------------------------------------------

integer CHANNEL = 1234;                     // The channel, where this script listening (number range −2147483648 to 2147483647)
string  PASSWORD = "Some Secure Password";  // Set here some secure password
vector COLOR_ENABLED  = <255, 0, 0>;        // Color (RGB) of the server, when the phantom doors is enabled (default Red color)
vector COLOR_DISABLED = <0, 255, 0>;        // Color (RGB) of the server, when the phantom doors is enabled (default Green color)

// ------------------------------------------------------------

integer status = 0;
integer listenHandle;

// ------------------------------------------------------------

setStatus(integer stat)
{
    status = stat;
    if (status == 1) {
        llSetColor(COLOR_ENABLED/255, ALL_SIDES);
    }
    else {
        llSetColor(COLOR_DISABLED/255, ALL_SIDES);
    }    
}   // End setStatus(integer stat)

// ------------------------------------------------------------

default
{
    state_entry()
    {
        llSetMemoryLimit(12228);
        llListenRemove(listenHandle);
        listenHandle = llListen(CHANNEL, "", "", "");
        llRegionSay(CHANNEL, PASSWORD + ":STATUS");
    }   // End state_entry()

    attach(key attached)
    {
        if ( attached != NULL_KEY ){
            llResetScript();
        }
    }   // End attach(key attached)

    changed(integer change)
    {
        if (change & CHANGED_OWNER) {
            llResetScript();
        }
    }   // End changed(integer change)

    touch_start(integer total_number)
    {
        if (status == 1) {
            llRegionSay(CHANNEL, PASSWORD + ":DISABLE");
        }
        else {
            llRegionSay(CHANNEL, PASSWORD + ":ENABLE");
        }
        status = ! status;
        setStatus(status);
        llSleep(0.5);
    }   // End touch_start(integer total_number)

    listen( integer channel, string name, key id, string message )
    {
        if (message == PASSWORD + ":ENABLE") {
            setStatus(1);
        }
        else if (message == PASSWORD + ":DISABLE") {
            setStatus(0);
        }
        else if (message == PASSWORD + ":STATUS") {
            llRegionSay(CHANNEL, PASSWORD + ":" + (string)status);
        }
        else if (message == PASSWORD + ":1") {
            setStatus(1);
        }
        else if (message == PASSWORD + ":0") {
            setStatus(0);
        }
    }   // End listen( integer channel, string name, key id, string message )

}   // End state default

// ------------------------------------------------------------
// ------------------------------------------------------------