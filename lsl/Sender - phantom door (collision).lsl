// *****************************************
// **  Sender - phantom door (collision)  **
// *****************************************
// License - GNU GPL
// Version: 1.1.0
// ------------------------------------------------------------
// ------------------------------------------------------------

// This script is intended for lever which change doors / cages to the phantom and back by 
// bumping at the lever.
// The lever must to be in max distance 10 meters
// A good idea is setup the CHANNEL and PASSWORD to the different values as the rest of this 
// system (these values of listener inside that lever door must to be the same).

// ------------------------------------------------------------
// ------------------------------------------------------------

integer CHANNEL = 1234;                     // The channel, where this script listening (number range −2147483648 to 2147483647)
string  PASSWORD = "Some Secure Password";  // Set here some secure password
float MAX_DISTANCE = 3.0;                   // The max. distance (meters) of the prim, which allow to change the settings of the doors
vector COLOR_ENABLED  = <255, 0, 0>;        // Color (RGB) of the prim, when the phantom doors is enabled (default Red color)
vector COLOR_DISABLED = <0, 255, 0>;        // Color (RGB) of the prim, when the phantom doors is enabled (default Green color)

// ------------------------------------------------------------

integer status = 0;

// ------------------------------------------------------------

default
{
    state_entry()
    {
        llSetMemoryLimit(8192);
        llSetColor(COLOR_DISABLED/255, ALL_SIDES);
        llSay(CHANNEL, PASSWORD + ":DISABLE");
    }   // End state_entry()

    on_rez(integer start_param)
    {
        llResetScript();
    }   // End on_rez(integer start_param)
    
    changed(integer change)
    {
        if (change & CHANGED_OWNER) {
            llResetScript();
        }
    }   // End changed(integer change)

    collision_start(integer num_detected)
    {
        if ( (llDetectedType(0) & AGENT) ) {
            if (status == 1) {
                llWhisper(CHANNEL, PASSWORD + ":DISABLE");
                llSetColor(COLOR_DISABLED/255, ALL_SIDES);
            }
            else {
                llWhisper(CHANNEL, PASSWORD + ":ENABLE");
                llSetColor(COLOR_ENABLED/255, ALL_SIDES);
            }
            status = ! status;
            llSleep(0.5);
        }
    }   // End collision_start(integer num_detected)

}   // End state default

// ------------------------------------------------------------
// ------------------------------------------------------------